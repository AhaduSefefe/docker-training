const rateLimit = require('express-rate-limit');

// eslint-disable-next-line no-unused-vars
console.log("this is rate limiter");
const rateLimiter = rateLimit({
        windowMs: 24 * 60 * 60 * 1000, // 24 hrs in milliseconds
        max: 1000,
        message: 'You have exceeded the 100 requests in 24 hrs limit!', 
        headers: true,
      });

module.exports = rateLimiter;