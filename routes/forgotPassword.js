/* eslint-disable no-undef */
const express = require('express');
const nodemailer = require('nodemailer');
const crypto = require('crypto');
const router = new express.Router();

const Customer = require('../models/customers');

router.post('/', async (req, res) => {
	const token = crypto.randomBytes(20).toString('hex');
	if(!req.body.email) return res.status(400).send("Enter an email");

	const customer = await Customer.findOneAndUpdate({email: req.body.email}, {
		resetPasswordToken: token,
		resetPasswordExpiry: Date.now() + 86400000
	});

	if(!customer) return res.status(404).send('Email not found');
	
	const transporter = nodemailer.createTransport({
		host: 'smtp.gmail.com',
		auth: {
			user: 'eyobmuktar4@gmail.com',
			pass: '922951539'
		},
		secureConnection: 'false',
		tls: {
			rejectUnauthorized: false
		}
	});

	const mailOptions = {
		from: 'eyobmuktar4@gmail.com',
		to:`${customer.email}`,
		subject: 'Link To Reset Password',
		text: 'Please click the following link or paste this into your browser to complete the process'
			+ `http://localhost:3002/reset/${token}`
	};

	console.log('sending email');

	transporter.sendMail(mailOptions, (err, response) => {
		if (err) {
			console.error('there is the res: ', err);
			res.status(500).send('Somethin went wrong!');
		} else {
			console.log('here is the res: ', response);
			res.status(200).send('recovery email sent');
		}
	});
});

module.exports = router;
